class CoolClass
   def do_stuff
      'my spec'
   end
end

describe CoolClass do
   context 'When testing the CoolClass class' do
      it 'should say \'my spec\' when we call the do_stuff method' do
         expect(CoolClass.new.do_stuff()).to eq 'my spec'
      end
   end
end
